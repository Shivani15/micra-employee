package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.relinns.micra.Activity.Main_screen;
import com.relinns.micra.Adapter.MyOderList_Adapter;
import com.relinns.micra.R;


/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class MyOrder_FRagment extends Fragment implements View.OnClickListener {

    ListView orderLV;
    LinearLayout allproductLL,lowLL,stockLL;
    TextView allproductTV,lowTV,stockTV;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.myorder_fragment,container,false);

        orderLV=(ListView)view.findViewById(R.id.orderLV);
        allproductLL=(LinearLayout)view.findViewById(R.id.allproductLL);

        lowLL=(LinearLayout)view.findViewById(R.id.lowLL);
        stockLL=(LinearLayout)view.findViewById(R.id.stockLL);

        allproductTV=(TextView)view.findViewById(R.id.allproductTV);
        lowTV=(TextView)view.findViewById(R.id.lowTV);
        stockTV=(TextView)view.findViewById(R.id.stockTV);

        allproductLL.setOnClickListener(this);
        lowLL.setOnClickListener(this);
        stockLL.setOnClickListener(this);

        ((Main_screen) getActivity()).settitleactivity("My Orders");

        Fragment fm = new ActiveOrder_Fragment();
        FragmentTransaction ftr = getFragmentManager().beginTransaction();
        ftr.replace(R.id.profileframeFL,fm,null);
        ftr.commit();


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.allproductLL:

                allproductLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                lowLL.setBackgroundColor(getResources().getColor(R.color.white));
                stockLL.setBackgroundColor(getResources().getColor(R.color.white));
                allproductTV.setTextColor(getResources().getColor(R.color.white));
                lowTV.setTextColor(getResources().getColor(R.color.black));
                stockTV.setTextColor(getResources().getColor(R.color.black));


                Fragment fm = new ActiveOrder_Fragment();
                FragmentTransaction ftr = getFragmentManager().beginTransaction();
                ftr.replace(R.id.profileframeFL,fm,null);
                ftr.commit();



                break;
            case R.id.lowLL:

                allproductLL.setBackgroundColor(getResources().getColor(R.color.white));
                lowLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                stockLL.setBackgroundColor(getResources().getColor(R.color.white));
                allproductTV.setTextColor(getResources().getColor(R.color.black));
                lowTV.setTextColor(getResources().getColor(R.color.white));
                stockTV.setTextColor(getResources().getColor(R.color.black));

                Fragment fm1 = new Transit_Fragment();
                FragmentTransaction ftr1 = getFragmentManager().beginTransaction();
                ftr1.replace(R.id.profileframeFL,fm1,null);
                ftr1.commit();
                break;

            case R.id.stockLL:

                allproductLL.setBackgroundColor(getResources().getColor(R.color.white));
                lowLL.setBackgroundColor(getResources().getColor(R.color.white));
                stockLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                allproductTV.setTextColor(getResources().getColor(R.color.black));
                lowTV.setTextColor(getResources().getColor(R.color.black));
                stockTV.setTextColor(getResources().getColor(R.color.white));

                Fragment fm2 = new History_Fragment();
                FragmentTransaction ftr2 = getFragmentManager().beginTransaction();
                ftr2.replace(R.id.profileframeFL,fm2,null);
                ftr2.commit();

                break;
        }

    }
}
