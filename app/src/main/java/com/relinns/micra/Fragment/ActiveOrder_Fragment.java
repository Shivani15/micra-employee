package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Adapter.MyOderList_Adapter;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class ActiveOrder_Fragment extends Fragment {

    ListView orderLV;
    MyOderList_Adapter myOderList_adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.active_fragment,container,false);

        orderLV=(ListView)view.findViewById(R.id.orderLV);

        myOderList_adapter=new MyOderList_Adapter(getActivity());
        orderLV.setAdapter(myOderList_adapter);
        return view;
    }
}
