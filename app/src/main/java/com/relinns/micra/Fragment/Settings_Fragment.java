package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.relinns.micra.Activity.About_us;
import com.relinns.micra.Activity.ReportFeedBack_Activity;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 17-07-2017.
 */

public class Settings_Fragment extends Fragment implements View.OnClickListener {
    LinearLayout aboutLL,reportLL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setingfragment, container, false);
        aboutLL = (LinearLayout) view.findViewById(R.id.aboutLL);
        reportLL = (LinearLayout) view.findViewById(R.id.reportLL);
        aboutLL.setOnClickListener(this);
        reportLL.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.aboutLL:
                Intent i = new Intent(getActivity(), About_us.class);
                startActivity(i);
                break;
            case R.id.reportLL:
                Intent i1 = new Intent(getActivity(), ReportFeedBack_Activity.class);
                startActivity(i1);
                break;
        }

    }
}
