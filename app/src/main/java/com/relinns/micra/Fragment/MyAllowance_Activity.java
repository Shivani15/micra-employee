package com.relinns.micra.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra.Activity.Main_screen;
import com.relinns.micra.Adapter.Allownace_Adapter;
import com.relinns.micra.Adapter.Message_Adapter;
import com.relinns.micra.R;

public class MyAllowance_Activity extends Fragment {
    ListView employlistLV;
    RelativeLayout backRL, addRL;
    Allownace_Adapter allownace_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.from(getActivity()).inflate(R.layout.activity_allowance, container, false);

        ((Main_screen)getActivity()).settitleactivity("My Allowance");
        ((Main_screen)getActivity()).showicon(false);
        ((Main_screen)getActivity()).addnewicon(true);

        employlistLV=(ListView)v.findViewById(R.id.employlistLV);
        allownace_adapter=new Allownace_Adapter(getActivity());
        employlistLV.setAdapter(allownace_adapter);

        return v;
    }
}
