package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.relinns.micra.Activity.Main_screen;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class SelectStore_Fragment extends Fragment implements View.OnClickListener {
    TextView vendorTV,mapTV;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.selectstore_fragment,container,false);
        ((Main_screen)getActivity()).settitleactivity("Select Store");
        vendorTV=(TextView)view.findViewById(R.id.vendorTV);
        mapTV=(TextView)view.findViewById(R.id.mapTV);
        vendorTV.setOnClickListener(this);
        mapTV.setOnClickListener(this);

        Fragment fm = new OnlySelectstoreFragment();
        FragmentTransaction ftr = getFragmentManager().beginTransaction();
        ftr.replace(R.id.profileframeFL,fm,null);
        ftr.commit();


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.vendorTV:
                vendorTV.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                mapTV.setBackgroundColor(getResources().getColor(R.color.white));
                vendorTV.setTextColor(getResources().getColor(R.color.white));
                mapTV.setTextColor(getResources().getColor(R.color.black));



                Fragment fm = new OnlySelectstoreFragment();
                FragmentTransaction ftr = getFragmentManager().beginTransaction();
                ftr.replace(R.id.profileframeFL,fm,null);
                ftr.commit();

                break;

            case R.id.mapTV:

                mapTV.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                vendorTV.setBackgroundColor(getResources().getColor(R.color.white));
                mapTV.setTextColor(getResources().getColor(R.color.white));
                vendorTV.setTextColor(getResources().getColor(R.color.black));


                Fragment fm1 = new OnlyMap_Fragment();
                FragmentTransaction ftr1 = getFragmentManager().beginTransaction();
                ftr1.replace(R.id.profileframeFL,fm1,null);
                ftr1.commit();
                break;
        }

    }
}
