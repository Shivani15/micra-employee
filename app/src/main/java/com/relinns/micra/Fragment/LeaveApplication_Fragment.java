package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Activity.Main_screen;
import com.relinns.micra.Adapter.LeaveApplicationHistory_Adapter;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class LeaveApplication_Fragment extends Fragment implements View.OnClickListener{
    ListView historyLV;
    LeaveApplicationHistory_Adapter leaveApplicationHistory_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.leaveapplication_fragment,container,false);
        ((Main_screen)getActivity()).settitleactivity("Leave Application");
        historyLV=(ListView)view.findViewById(R.id.historyLV);
        leaveApplicationHistory_adapter=new LeaveApplicationHistory_Adapter(getActivity());
        historyLV.setAdapter(leaveApplicationHistory_adapter);
        return view;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){


        }
    }
}
