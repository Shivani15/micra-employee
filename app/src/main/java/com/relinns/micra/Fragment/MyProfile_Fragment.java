package com.relinns.micra.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.relinns.micra.Activity.Add_Address_Activity;
import com.relinns.micra.Activity.Edit_Address;
import com.relinns.micra.Activity.Main_screen;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 17-07-2017.
 */

public class MyProfile_Fragment extends Fragment implements View.OnClickListener {

    RelativeLayout addnewRL;
    LinearLayout editLL,editLL2;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.myprofile_fragment,container,false);
        ((Main_screen) getActivity()).settitleactivity("My Profile");
        addnewRL=(RelativeLayout)view.findViewById(R.id.addnewRL);
        editLL=(LinearLayout) view.findViewById(R.id.editLL);
        editLL2=(LinearLayout) view.findViewById(R.id.editLL2);
        addnewRL.setOnClickListener(this);
        editLL.setOnClickListener(this);
        editLL2.setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.addnewRL:

                Intent intent=new Intent(getActivity(),Add_Address_Activity.class);
                startActivity(intent);

                break;

            case R.id.editLL:
                Intent intent1=new Intent(getActivity(),Edit_Address.class);
                startActivity(intent1);
                break;


            case R.id.editLL2:
                Intent intent2=new Intent(getActivity(),Edit_Address.class);
                startActivity(intent2);

                break;
        }

    }
}
