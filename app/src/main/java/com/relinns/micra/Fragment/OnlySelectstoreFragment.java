package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Adapter.OnlySelectStore_Adapter;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class OnlySelectstoreFragment extends Fragment {
    ListView selectstoreLV;
    OnlySelectStore_Adapter onlySelectStore_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.onlyselectstore_fragment,container,false);



        selectstoreLV=(ListView)view.findViewById(R.id.selectstoreLV);
        onlySelectStore_adapter=new OnlySelectStore_Adapter(getActivity());
        selectstoreLV.setAdapter(onlySelectStore_adapter);
        return  view;
    }
}
