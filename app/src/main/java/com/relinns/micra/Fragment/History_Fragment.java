package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra.Adapter.HistoryOrder_Adapter;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class History_Fragment extends Fragment {

    ListView orderLV;
    HistoryOrder_Adapter historyOrder_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.history_fragment,container,false);
        orderLV=(ListView)view.findViewById(R.id.orderLV);

        historyOrder_adapter=new HistoryOrder_Adapter(getActivity());
        orderLV.setAdapter(historyOrder_adapter);


        return  view;
    }
}
