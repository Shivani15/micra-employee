package com.relinns.micra.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.relinns.micra.Activity.Main_screen;
import com.relinns.micra.Adapter.My_customerAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.Customer_Model;

import java.util.ArrayList;

/**
 * Created by admin on 07-07-2017.
 */
public class My_customer extends Fragment {
    ArrayList<Customer_Model> customer_data;
    RecyclerView my_customerlist;
    My_customerAdapter customerAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.from(getActivity()).inflate(R.layout.activity_my_customers,container,false);
        my_customerlist = (RecyclerView)v. findViewById(R.id.my_customerlist);
        ((Main_screen)getActivity()).settitleactivity("Customers");
        ((Main_screen)getActivity()).showicon(true);
        ((Main_screen)getActivity()).addnewicon(false);
        customer_data=new ArrayList<>();
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "1"));
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "0"));
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "1"));


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        my_customerlist.setLayoutManager(mLayoutManager);
        customerAdapter = new My_customerAdapter(getActivity(), customer_data);
        my_customerlist.setAdapter(customerAdapter);
        return v;
    }
}
