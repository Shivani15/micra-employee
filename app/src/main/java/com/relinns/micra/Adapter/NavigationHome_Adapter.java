package com.relinns.micra.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.relinns.micra.Activity.Main_screen;
import com.relinns.micra.Activity.Order_placed;
import com.relinns.micra.R;
import com.relinns.micra.model.NavigationHome_Model;

import java.util.ArrayList;

/**
 * Created by admin on 07-07-2017.
 */
public class NavigationHome_Adapter extends BaseAdapter {
    ArrayList<NavigationHome_Model> navigationHome_models;
    Context context;
    public NavigationHome_Adapter(ArrayList<NavigationHome_Model> navigationHome_models, Main_screen context) {
        this.navigationHome_models=navigationHome_models;
        this.context=context;
    }

    @Override
    public int getCount() {
        return navigationHome_models.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        //  rowView = inflater.inflate(R.layout.modelnavigation, null);
        // rowView = inflater.inflate(R.layout.modelnavigation,null,false);
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.navigation_model, parent, false);
        holder.tv = (TextView) convertView.findViewById(R.id.textTV);
        holder.img = (ImageView) convertView.findViewById(R.id.imageIV);
        holder.switchbutton = (Switch) convertView.findViewById(R.id.switchbutton);



        String name = navigationHome_models.get(position).getName();
        int image = navigationHome_models.get(position).getImage();
        holder.tv.setText(name);
        holder.img.setImageResource(image);
        return convertView;
    }

    public class Holder {
        TextView tv;
        ImageView img;
        Switch switchbutton;
    }

}
