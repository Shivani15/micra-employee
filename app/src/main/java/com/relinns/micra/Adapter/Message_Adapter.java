package com.relinns.micra.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Activity.Chat;
import com.relinns.micra.R;

/**
 * Created by intel on 6/30/2017.
 */

public class Message_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    public Message_Adapter(Context context) {
        this.context=context;
        activity=(Activity)context;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.employeelist_item, parent, false);
        TextView justTV= (TextView)convertView.findViewById(R.id.justTV);
        RelativeLayout  message_layout=(RelativeLayout)convertView.findViewById(R.id.message_layout);
        message_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, Chat.class);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });


        return convertView;
    }
    public class Holder {
        TextView tv;
        ImageView img;
        RelativeLayout layotclickRL,editRL,message_layout;
    }
}
