package com.relinns.micra.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Activity.Chat;
import com.relinns.micra.Activity.MessageActivity;
import com.relinns.micra.Activity.My_customers;
import com.relinns.micra.Fragment.My_customer;
import com.relinns.micra.R;
import com.relinns.micra.model.Customer_Model;

import java.util.ArrayList;

/**
 * Created by admin on 06-07-2017.
 */
public class My_customerAdapter extends RecyclerView.Adapter<My_customerAdapter.ViewHolder> {
    Context context;
    ArrayList<Customer_Model> list;
    Activity activity;
    public My_customerAdapter(Context my_customers, ArrayList<Customer_Model> customer_data) {
        this.context=my_customers;
        this.list=customer_data;
        activity=(Activity) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.design_customers,parent,false);



        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       holder.user_image.setImageResource(list.get(position).getOffline_user());
        holder.user_name.setText(list.get(position).getUser_name());
        holder.user_country.setText(list.get(position).getPlace());
        /*if(list.get(position).getOrder().equals("0"))
        {
            holder.place_order.setVisibility(View.GONE);
        }
        else
        {
            holder.place_order.setVisibility(View.VISIBLE);
        }*/
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView user_image;
        TextView user_name, user_country;
        RelativeLayout place_order, chat;

        public ViewHolder(View itemView) {
            super(itemView);
            user_image=(ImageView)itemView.findViewById(R.id.user_image);
            user_name=(TextView)itemView.findViewById(R.id.customer_name);
            user_country=(TextView)itemView.findViewById(R.id.customer_city);
           // place_order=(RelativeLayout)itemView.findViewById(R.id.place_order);
            chat=(RelativeLayout)itemView.findViewById(R.id.chat);

            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, Chat.class);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            });

        }
    }
}
