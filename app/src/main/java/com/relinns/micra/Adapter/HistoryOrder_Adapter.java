package com.relinns.micra.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.relinns.micra.Activity.OrderDetailActivity;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class HistoryOrder_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    public HistoryOrder_Adapter(FragmentActivity context) {

        this.context=context;
        activity=(Activity)context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder=new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.historylist_item, parent, false);

        holder.prouctconditionTV=(TextView)convertView.findViewById(R.id.prouctconditionTV);
        holder.layotLL=(LinearLayout)convertView.findViewById(R.id.layotLL);
        if (position==0){
            holder.prouctconditionTV.setText("Complete");
        }
        if (position==1){
            holder.prouctconditionTV.setText("Complete");
        }
       /* if (position==2){
            holder.prouctconditionTV.setText("Completed");
        }
        if (position==3){
            holder.prouctconditionTV.setText("Transit");
        }
        if (position==4){
            holder.prouctconditionTV.setText("Refund Request");
        }*/

        holder.layotLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, OrderDetailActivity.class);
                intent.putExtra("order_type","History");
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter,R.anim.exit);

            }
        });

      /*  holder.layotLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position==0){

                    Intent intent=new Intent(context, OrderDetail_Pending_Activity.class);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);

                }
              else  if (position==1){
                    Intent intent=new Intent(context, OrderDetailAccepted_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
               else if(position==2){
                    Intent intent=new Intent(context, OrderDetailComplete_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                else if (position==3){
                    Intent intent=new Intent(context, OrderDetailTransit_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                else{
                    Intent intent=new Intent(context, OrderRefundRequest_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });

*/

        return convertView;
    }

    public class Holder {

        LinearLayout layotLL,editRL;
        TextView prouctconditionTV;
    }
}

