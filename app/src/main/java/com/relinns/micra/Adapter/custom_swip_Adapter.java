package com.relinns.micra.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.relinns.micra.R;
import com.relinns.micra.model.image_model;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Relinns-Technologies on 3/23/2017.
 */

public class custom_swip_Adapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<image_model> arrayList;

    public custom_swip_Adapter(Context context, ArrayList<image_model> arrayList){
        Log.d("array",arrayList.toString());
        this.context = context;
        this.arrayList = arrayList;
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
     //   Log.d("size",arrayList.size()+"");
        return arrayList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View item_view = layoutInflater.inflate(R.layout.design_view_images,container,false);
        ImageView imageView = (ImageView)item_view.findViewById(R.id.imageView);
        Log.d("image", arrayList.get(position).getUrl()+"");


            Picasso.with(context)
                    .load("sjkfbksbd")
                    .placeholder(arrayList.get(position).getUrl())   // optional
                    .error(arrayList.get(position).getUrl())      // optional
                    //  .resize(400, 400)                        // optional

                    .into(imageView);


        container.addView(item_view);

        return item_view;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }
}
