package com.relinns.micra.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Activity.StoreProductActivity;
import com.relinns.micra.Activity.Upload_placeOrder;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class OnlySelectStore_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    public OnlySelectStore_Adapter(FragmentActivity context) {
        this.context=context;
        activity=(Activity)context;

    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.onlystore_item, parent, false);

        holder.browseRL=(RelativeLayout)convertView.findViewById(R.id.browseRL1);
        holder.orderRL=(RelativeLayout)convertView.findViewById(R.id.orderRL);
        holder.browseRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, StoreProductActivity.class);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        holder.orderRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Upload_placeOrder.class);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        return convertView;
    }

    public class Holder {
        RelativeLayout browseRL,orderRL;

    }
}
