package com.relinns.micra.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.relinns.micra.Activity.Customer_cart;
import com.relinns.micra.R;
import com.relinns.micra.model.Cart_Model;

import java.util.ArrayList;

/**
 * Created by admin on 06-07-2017.
 */
public class Customer_cartAdapter extends RecyclerView.Adapter<Customer_cartAdapter.View_holder> {
    Customer_cart context;
    ArrayList<Cart_Model> list;
    public Customer_cartAdapter(Customer_cart customer_cart, ArrayList<Cart_Model> cart_list) {
        this.context=customer_cart;
        this.list=cart_list;
    }

    @Override
    public View_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.design_customer_cart,parent,false);
        return new View_holder(v);
    }

    @Override
    public void onBindViewHolder(View_holder holder, int position) {

      /*  holder.cart_productName.setText(list.get(position).getProduct_name());
        holder.cart_vendorName.setText(list.get(position).getVendor_name());
        holder.cart_productPrice.setText(list.get(position).getPrice());
        holder.cart_productOffer.setText(list.get(position).getOffer());
        holder.cart_quantity.setText(list.get(position).getQuantity());
        holder.cart_image.setImageResource(list.get(position).getPlace_holder());*/
    }



    @Override
    public int getItemCount() {
        return 2;
    }

    public class View_holder extends RecyclerView.ViewHolder {
        ImageView cart_image;
        TextView cart_productName, cart_vendorName, cart_productPrice, cart_productOffer, cart_quantity;
        public View_holder(View itemView) {
            super(itemView);
            cart_image=(ImageView)itemView.findViewById(R.id.cart_image);
            cart_productName=(TextView)itemView.findViewById(R.id.cart_productName);
            cart_vendorName=(TextView)itemView.findViewById(R.id.cart_vendorName);
            cart_quantity=(TextView)itemView.findViewById(R.id.cart_quantity);
        }
    }
}
