package com.relinns.micra.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.relinns.micra.R;

public class Forgot_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_);
    }

    public void backpress(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        onBackPressed();
        Forgot_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }



}
