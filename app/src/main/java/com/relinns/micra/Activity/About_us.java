package com.relinns.micra.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.relinns.micra.R;

public class About_us extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }

    public void backpress(View v) {
        onBackPressed();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        About_us.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);


    }
}
