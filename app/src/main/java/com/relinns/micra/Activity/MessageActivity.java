package com.relinns.micra.Activity;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra.Adapter.Message_Adapter;
import com.relinns.micra.R;

public class MessageActivity extends Fragment  {
    ListView employlistLV;
    RelativeLayout backRL;
    Message_Adapter message_adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.from(getActivity()).inflate(R.layout.activity_message, container, false);
        ((Main_screen)getActivity()).settitleactivity("Message");
        ((Main_screen)getActivity()).showicon(false);
        ((Main_screen)getActivity()).addnewicon(false);
        employlistLV = (ListView) v.findViewById(R.id.employlistLV);
        message_adapter = new Message_Adapter(getActivity());
        employlistLV.setAdapter(message_adapter);
        return v;
    }

}
