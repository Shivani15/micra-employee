package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class ProductDetail_Activity extends Activity implements View.OnClickListener {

    RelativeLayout headerRL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_productdetail);
        headerRL=(RelativeLayout)findViewById(R.id.headerRL);
        headerRL.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch ( v.getId()){
            case R.id.headerRL:

                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ProductDetail_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
