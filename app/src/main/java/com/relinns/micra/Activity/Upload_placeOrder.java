package com.relinns.micra.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.relinns.micra.R;

import static com.relinns.micra.R.id.productIV;

public class Upload_placeOrder extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout selectimage_RL,setimageRL;
    private static final int GALLERY_REQUEST = 1999;
    ImageView setimageIV;
    Uri selectedImageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_place_order);
        selectimage_RL=(RelativeLayout)findViewById(R.id.selectimage_RL);
        setimageRL=(RelativeLayout)findViewById(R.id.setimageRL);
        setimageIV=(ImageView)findViewById(R.id.setimageIV);
        selectimage_RL.setOnClickListener(this);
    }
    public void backpress(View v)
    {
     onBackPressed();
    }
    public void place_order(View v) {
        Intent i= new Intent(Upload_placeOrder.this,Customer_cart.class);
        startActivity(i);
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Upload_placeOrder.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.selectimage_RL:

                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_REQUEST);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, GALLERY_REQUEST);
                }

                break;
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
     if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            if (resultCode == RESULT_OK) {
                if (requestCode == GALLERY_REQUEST) {
                    // Get the url from data
                    selectedImageUri = data.getData();


                    if (!selectedImageUri.equals("")) {

                        Glide
                                .with(Upload_placeOrder.this)
                                .load(selectedImageUri)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .centerCrop()
                                .crossFade()
                                .into(setimageIV);

                    }

                }
            }

        } else {
            Toast.makeText(this, "no response", Toast.LENGTH_SHORT).show();
        }
    }

}
