package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.relinns.micra.R;

public class Edit_Address extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__address);
    }

    public void backpress(View v)
    {
        onBackPressed();
}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Edit_Address.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
