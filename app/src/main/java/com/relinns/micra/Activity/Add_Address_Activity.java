package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.relinns.micra.R;

public class Add_Address_Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__address);
    }

    public void backpress(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Add_Address_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    public void save(View v) {
        Intent i = new Intent(Add_Address_Activity.this, Edit_Address.class);
        startActivity(i);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

}
