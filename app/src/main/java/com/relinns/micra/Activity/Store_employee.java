package com.relinns.micra.Activity;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.relinns.micra.Adapter.My_customerAdapter;
import com.relinns.micra.Adapter.Store_employeeAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.Customer_Model;

import java.util.ArrayList;

public class Store_employee extends Fragment {
    ArrayList<Customer_Model> employee_data;
    RecyclerView employee_list;
    Store_employeeAdapter employeeAdapter;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.from(getActivity()).inflate(R.layout.activity_store_employee,container,false);
        employee_list = (RecyclerView)v. findViewById(R.id.store_employee_list);
        employee_data = new ArrayList<>();
        ((Main_screen)getActivity()).settitleactivity("Store Employee");
        ((Main_screen)getActivity()).showicon(false);
        ((Main_screen)getActivity()).addnewicon(false);
        employee_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "Online"));
        employee_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "Offline"));
        employee_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "Online"));
        employee_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "Online"));
        employee_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "Online"));


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        employee_list.setLayoutManager(mLayoutManager);
        employeeAdapter = new Store_employeeAdapter(getActivity(), employee_data);
        employee_list.setAdapter(employeeAdapter);
        return v;
    }
}
