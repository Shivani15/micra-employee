package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.relinns.micra.R;

public class Edit_profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
    }
    public  void  save(View v)
    {
        Intent i= new Intent(Edit_profile.this,Add_Address_Activity.class);
        startActivity(i);
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }
    public  void  backpress(View v)
    {
     onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Edit_profile.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
