package com.relinns.micra.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Adapter.NavigationHome_Adapter;
import com.relinns.micra.R;
import com.relinns.micra.model.NavigationHome_Model;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Order_placed extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placed);

    }
    public  void place_another(View v)
    {
        Intent i= new Intent(Order_placed.this,StoreProductActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }
    public void backpress(View v)
    {
      onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Order_placed.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
