package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.relinns.micra.R;
import com.tapadoo.alerter.Alerter;

public class Login_Activity extends AppCompatActivity implements View.OnClickListener {
    EditText emailET, passwordET;
    LinearLayout loginLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        emailET = (EditText) findViewById(R.id.emailET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        loginLL = (LinearLayout) findViewById(R.id.loginLL);
        loginLL.setOnClickListener(this);

    }

    public void open_reg(View view) {
        Intent intent = new Intent(Login_Activity.this, Signup_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void open_forgot(View view) {
        Intent intent = new Intent(Login_Activity.this, Forgot_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    public void login(View view) {
        Intent intent = new Intent(Login_Activity.this, Main_screen.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }


    public void backpress(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Login_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginLL:
                Intent intent = new Intent(Login_Activity.this, Main_screen.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
        }
    }
}
