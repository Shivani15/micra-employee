package com.relinns.micra.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.relinns.micra.R;

public class Track_order extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order);
    }
    public void backpress(View v)
    {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Track_order.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
