package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 21-07-2017.
 */

public class OrderDetailActivity extends Activity implements View.OnClickListener {
    String order_type;
    RelativeLayout firstlayout,secondlayout,thirdlayout,fourthlayout,backRL;
    TextView firsttext,secondtext,thirdtext,fourthtext,approvalTV,processingTV,trasitTV,deliveredTV;
    View firstview,secondview,thirdview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetail);

        firstlayout=(RelativeLayout)findViewById(R.id.firstlayout);
        secondlayout=(RelativeLayout)findViewById(R.id.secondlayout);
        thirdlayout=(RelativeLayout)findViewById(R.id.thirdlayout);
        fourthlayout=(RelativeLayout)findViewById(R.id.fourthlayout);

        backRL=(RelativeLayout)findViewById(R.id.backRL);

        firsttext=(TextView) findViewById(R.id.firsttext);
        secondtext=(TextView) findViewById(R.id.secondtext);
        thirdtext=(TextView) findViewById(R.id.thirdtext);
        fourthtext=(TextView) findViewById(R.id.fourthtext);

        approvalTV=(TextView) findViewById(R.id.approvalTV);
        processingTV=(TextView) findViewById(R.id.processingTV);
        trasitTV=(TextView) findViewById(R.id.trasitTV);
        deliveredTV=(TextView) findViewById(R.id.deliveredTV);

        firstview=(View) findViewById(R.id.firstview);
        secondview=(View) findViewById(R.id.secondview);
        thirdview=(View) findViewById(R.id.thirdview);

        backRL.setOnClickListener(this);

        order_type=getIntent().getStringExtra("order_type");

        if (order_type.equalsIgnoreCase("Accept")){

            firstlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            secondlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            thirdlayout.setBackground(getResources().getDrawable(R.drawable.grey_circle));
            fourthlayout.setBackground(getResources().getDrawable(R.drawable.grey_circle));

            approvalTV.setTextColor(getResources().getColor(R.color.micra));
            processingTV.setTextColor(getResources().getColor(R.color.micra));

            firstview.setBackgroundColor(getResources().getColor(R.color.micra));
            secondview.setBackgroundColor(getResources().getColor(R.color.grey));
            thirdview.setBackgroundColor(getResources().getColor(R.color.grey));


        }
        else if (order_type.equalsIgnoreCase("Transit")){
            firstlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            secondlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            thirdlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            fourthlayout.setBackground(getResources().getDrawable(R.drawable.grey_circle));

            approvalTV.setTextColor(getResources().getColor(R.color.micra));
            processingTV.setTextColor(getResources().getColor(R.color.micra));
            trasitTV.setTextColor(getResources().getColor(R.color.micra));

            firstview.setBackgroundColor(getResources().getColor(R.color.micra));
            secondview.setBackgroundColor(getResources().getColor(R.color.micra));
            thirdview.setBackgroundColor(getResources().getColor(R.color.grey));

        }
        else if (order_type.equalsIgnoreCase("History")){
            firstlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            secondlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            thirdlayout.setBackground(getResources().getDrawable(R.drawable.circle));
            fourthlayout.setBackground(getResources().getDrawable(R.drawable.circle));

            approvalTV.setTextColor(getResources().getColor(R.color.micra));
            processingTV.setTextColor(getResources().getColor(R.color.micra));
            trasitTV.setTextColor(getResources().getColor(R.color.micra));
            deliveredTV.setTextColor(getResources().getColor(R.color.micra));


            firstview.setBackgroundColor(getResources().getColor(R.color.micra));
            secondview.setBackgroundColor(getResources().getColor(R.color.micra));
            thirdview.setBackgroundColor(getResources().getColor(R.color.micra));

        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        OrderDetailActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
