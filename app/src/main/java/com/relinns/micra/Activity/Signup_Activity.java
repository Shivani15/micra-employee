package com.relinns.micra.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.relinns.micra.Adapter.CountryCodeAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.CountryCode_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

public class Signup_Activity extends AppCompatActivity {

    Dialog dialog;
    ListView listView;
    String items[],items1[];
    TextView c_code,line;
    ArrayList<CountryCode_Model>models;
    CountryCodeAdapter countryCodeAdapter;
    EditText searchET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_);
        models=new ArrayList<CountryCode_Model>();

        code();
        c_code = (TextView)findViewById(R.id.c_code);
        line = (TextView)findViewById(R.id.line);
        line_color();

    }


    public void open_verification(View view){

        Intent intent = new Intent(Signup_Activity.this,verification_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }


    public void backpress(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Signup_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    void line_color(){

        SpannableString SpanString = new SpannableString("By clicking 'Register' you agree to Terms & Conditions and Privacy Policy");


        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 12, 22, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 36, 54, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 59, 73, 0);

        line.setMovementMethod(LinkMovementMethod.getInstance());
        line.setText(SpanString, TextView.BufferType.SPANNABLE);
        line.setSelected(true);


    }

    void code(){

        try {
            JSONArray jsonArray=new JSONArray(loadJSONFromAsset());
            items=new String[jsonArray.length()];
            items1=new String[jsonArray.length()];
            for(int i=0;i<jsonArray.length();i++){
                JSONObject object=jsonArray.getJSONObject(i);
                Log.d("response",object+"");
                items[i]="+"+object.getString("code")+" "+object.getString("name");
                items1[i] = "+"+object.getString("code");

                String name=object.getString("name");
                String code=object.getString("code");
                models.add(new CountryCode_Model(name,code));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("isd.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public void open_code(View view)
    {
        // DialogWithBlurredBackgroundLauncher dialogWithBlurredBackgroundLauncher = new DialogWithBlurredBackgroundLauncher(MainActivity.this);
        dialog = new Dialog(Signup_Activity.this);
        // Include dialog.xml file

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes(); // retrieves the windows attributes

        lp.dimAmount=0.0f; // sets the dimming amount to zero

        dialog.getWindow().setAttributes(lp); // sets the updated windows attributes

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.countrydesign);



        listView=(ListView) dialog.findViewById(R.id.list);
        searchET=(EditText) dialog.findViewById(R.id.searchET);
        //listView.setFastScrollEnabled(true);

        code();
        countryCodeAdapter =new CountryCodeAdapter(Signup_Activity.this,models);
        listView.setAdapter(countryCodeAdapter);

      /*  ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(itemsAdapter);*/

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                c_code.setText(models.get(position).getCode());
                dialog.dismiss();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                countryCodeAdapter =new CountryCodeAdapter(Signup_Activity.this,models);
                listView.setAdapter(countryCodeAdapter);
            }
        });

        searchET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                //  Toast.makeText(getActivity(), cs.toString(), Toast.LENGTH_LONG).show();
                String text = searchET.getText().toString().toLowerCase(Locale.getDefault());
                countryCodeAdapter.filter(text);


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

       /* cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });*/
        dialog.show();
        // dialogWithBlurredBackgroundLauncher.showDialog(dialog);
    }




}
