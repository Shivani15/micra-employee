package com.relinns.micra.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.relinns.micra.Adapter.Customer_cartAdapter;
import com.relinns.micra.Adapter.My_customerAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.Customer_Model;

import java.util.ArrayList;

public class My_customers extends AppCompatActivity {
    ArrayList<Customer_Model> customer_data;
    RecyclerView my_customerlist;
    My_customerAdapter customerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_customers);
        my_customerlist = (RecyclerView) findViewById(R.id.my_customerlist);
        customer_data = new ArrayList<>();
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "0"));
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "0"));
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "1"));
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "0"));
        customer_data.add(new Customer_Model(R.drawable.offline_user, "John Doe", "City, Country", "1"));

//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(My_customers.this);
//        my_customerlist.setLayoutManager(mLayoutManager);
//        customerAdapter = new My_customerAdapter(My_customers.this, customer_data);
//        my_customerlist.setAdapter(customerAdapter);
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        My_customers.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
