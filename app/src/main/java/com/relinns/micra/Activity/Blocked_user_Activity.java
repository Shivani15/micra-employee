package com.relinns.micra.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.relinns.micra.R;

public class Blocked_user_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_user);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Blocked_user_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
