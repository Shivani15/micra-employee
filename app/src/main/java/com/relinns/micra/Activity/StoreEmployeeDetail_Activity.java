package com.relinns.micra.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class StoreEmployeeDetail_Activity extends Activity implements View.OnClickListener {

    RelativeLayout trackemployeeRL,backRL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storeemployeedetail);
        trackemployeeRL=(RelativeLayout)findViewById(R.id.trackemployeeRL);
        backRL=(RelativeLayout)findViewById(R.id.backRL);

        trackemployeeRL.setOnClickListener(this);
        backRL.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();

                break;

            case R.id.trackemployeeRL:

                Intent intent=new Intent(StoreEmployeeDetail_Activity.this,Track_order.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StoreEmployeeDetail_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
