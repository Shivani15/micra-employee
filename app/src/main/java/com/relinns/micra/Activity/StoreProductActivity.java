package com.relinns.micra.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra.Adapter.StoreProduct_Adapter;
import com.relinns.micra.R;

/**
 * Created by Relinns Technologies on 18-07-2017.
 */

public class StoreProductActivity extends Activity implements View.OnClickListener {
    ListView storeproductLV;
    StoreProduct_Adapter storeProduct_adapter;
    RelativeLayout backRL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storeproduct);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        storeproductLV=(ListView)findViewById(R.id.storeproductLV);
        storeProduct_adapter=new StoreProduct_Adapter(StoreProductActivity.this);
        storeproductLV.setAdapter(storeProduct_adapter);

        backRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StoreProductActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
