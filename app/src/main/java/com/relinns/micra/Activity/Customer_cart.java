package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.relinns.micra.Adapter.Customer_cartAdapter;
import com.relinns.micra.R;
import com.relinns.micra.model.Cart_Model;

import java.util.ArrayList;

public class Customer_cart extends AppCompatActivity {
ArrayList<Cart_Model> cart_list;
    Customer_cartAdapter cartAdapter;
    RecyclerView cart_listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_cart);
        cart_list=new ArrayList<>();
        cart_listView=(RecyclerView)findViewById(R.id.cart_listView);
        cart_list.add(new Cart_Model(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new Cart_Model(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new Cart_Model(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new Cart_Model(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new Cart_Model(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new Cart_Model(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        cart_list.add(new Cart_Model(R.drawable.placeholder,"Product Name","Vendor Name","575.60","Buy 3 Get 1 free","2"));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Customer_cart.this);
        cart_listView.setLayoutManager(mLayoutManager);
        cartAdapter=new Customer_cartAdapter(Customer_cart.this,cart_list);
        cart_listView.setAdapter(cartAdapter);
    }
    public void checkout(View v)
    {
        Intent i= new Intent(Customer_cart.this,Checkout.class);
        startActivity(i);
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }
    public void backpress(View v)
    {
  onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Customer_cart.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
