package com.relinns.micra.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.relinns.micra.R;

public class Payment extends AppCompatActivity {
    RadioButton walletRB,cashRB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        walletRB=(RadioButton)findViewById(R.id.walletRB);
        cashRB=(RadioButton)findViewById(R.id.cashRB);

        cashRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    walletRB.setChecked(false);
                }
            }
        });

        walletRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    cashRB.setChecked(false);
                }
            }
        });
    }
    public void pay_money(View v)
    {
        Intent i= new Intent(Payment.this,Order_placed.class);
        startActivity(i);
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }
    public void backpress(View v)
    {
       onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Payment.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
