package com.relinns.micra.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra.Adapter.NavigationHome_Adapter;
import com.relinns.micra.Fragment.LeaveApplication_Fragment;
import com.relinns.micra.Fragment.MyAllowance_Activity;
import com.relinns.micra.Fragment.MyOrder_FRagment;
import com.relinns.micra.Fragment.MyProfile_Fragment;
import com.relinns.micra.Fragment.My_customer;
import com.relinns.micra.Fragment.SelectStore_Fragment;
import com.relinns.micra.Fragment.Settings_Fragment;
import com.relinns.micra.Fragment.WebMicra_Fragment;
import com.relinns.micra.R;
import com.relinns.micra.model.NavigationHome_Model;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Main_screen extends AppCompatActivity implements View.OnClickListener {
    ListView mDrawerList;
    RelativeLayout layout, add_new, notification, search, menu;
    Toolbar mToolbar;
    ActionBar actionBar;
    DrawerLayout mDrawerLayout;
    TextView titletext, editTV, adnew_emplyTV, adnew_leaveTV;
    ArrayList<NavigationHome_Model> navigationHome_models;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private float lastTranslate = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        add_new = (RelativeLayout) findViewById(R.id.add);
        menu = (RelativeLayout) findViewById(R.id.menu);
        notification = (RelativeLayout) findViewById(R.id.notification);
        search = (RelativeLayout) findViewById(R.id.search);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        layout = (RelativeLayout) findViewById(R.id.layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        titletext = (TextView) findViewById(R.id.titleTV1);
        editTV = (TextView) findViewById(R.id.editTV);
        adnew_leaveTV = (TextView) findViewById(R.id.adnew_leaveTV);

        adnew_emplyTV = (TextView) findViewById(R.id.adnew_emplyTV);
        adnew_emplyTV.setOnClickListener(this);
        adnew_leaveTV.setOnClickListener(this);
        actionBar = getSupportActionBar();
        setSupportActionBar(mToolbar);
        navigationHome_models = new ArrayList<NavigationHome_Model>();

        navigationHome_models.add(new NavigationHome_Model(R.drawable.home, "Home"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.my_order, "My Orders"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.my_profile, "My Profile"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.my_customers, "My Customer"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.message, "Messages"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.store_employee, "Store Employee"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.leaveapp, "My Leave"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.allowance, "My Allowance"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.webmicra, "Login Web Micra"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.settings, "Settings"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.logout, "Logout"));


        LayoutInflater inflater = getLayoutInflater();

        View listHeaderView = inflater.inflate(R.layout.header_list, null, false);

        ImageView drawer_profileIV = (CircleImageView) listHeaderView.findViewById(R.id.drawer_profileIV);
        TextView drawer_profilenameTV = (TextView) listHeaderView.findViewById(R.id.drawer_profilenameTV);

        // Glide.with(My_Products_Activity.this).load(persnl_image).centerCrop().crossFade().into(drawer_profileIV);
        //  drawer_profilenameTV.setText(persnl_name);

        mDrawerList.addHeaderView(listHeaderView);


        mDrawerList.setAdapter(new NavigationHome_Adapter(navigationHome_models, Main_screen.this));


        home();


        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                null,  /* nav drawer icon to replace 'Up' caret */

                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                InputMethodManager inputMethodManager = (InputMethodManager) Main_screen.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(Main_screen.this.getCurrentFocus().getWindowToken(), 0);

                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mTitle);

                InputMethodManager inputMethodManager = (InputMethodManager) Main_screen.this
                        .getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(Main_screen.this.getCurrentFocus().getWindowToken(), 0);

                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }


            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                float moveFactor = (mDrawerList.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    layout.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    layout.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
          /*  super.onDrawerOpened(drawerView);
            invalidateOptionsMenu();*/
        };
        getSupportActionBar().setTitle("");
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        // getSupportActionBar().setHomeAsUpIndicator(R.drawable.navigation);
        mToolbar.setTitle("");
        mToolbar.setSubtitle("");

        search.setOnClickListener(this);
        notification.setOnClickListener(this);
        add_new.setOnClickListener(this);
        menu.setOnClickListener(this);
        editTV.setOnClickListener(this);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(Navigation_Home_First.this, "=" + position, Toast.LENGTH_SHORT).show();
                switch (position) {
                    case 0:

                        break;

                    case 1:
                        mDrawerLayout.closeDrawers();
                        adnew_emplyTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        notification.setVisibility(View.VISIBLE);
                        search.setVisibility(View.VISIBLE);
                        add_new.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        home();
                        break;

                    case 2:
                        mDrawerLayout.closeDrawers();
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        adnew_emplyTV.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);

                        omyorder_fragment();

                        break;

                    case 3:
                        mDrawerLayout.closeDrawers();
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        adnew_emplyTV.setVisibility(View.GONE);
                        editTV.setVisibility(View.VISIBLE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        my_profile();
                        break;

                    case 4:
                        mDrawerLayout.closeDrawers();
                        notification.setVisibility(View.VISIBLE);
                        search.setVisibility(View.VISIBLE);
                        add_new.setVisibility(View.VISIBLE);
                        adnew_emplyTV.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        my_customer();
                       /* Intent intent = new Intent(My_Products_Activity.this, Add_StoreAddress_Activity.class);
                        startActivity(intent);*/
                        break;

                    case 5:
                        mDrawerLayout.closeDrawers();
                        adnew_emplyTV.setVisibility(View.GONE);
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        messages();
                        break;

                    case 6:
                        mDrawerLayout.closeDrawers();
                        adnew_emplyTV.setVisibility(View.GONE);
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        store_employee();
                        break;

                    case 7:
                        mDrawerLayout.closeDrawers();
                        adnew_emplyTV.setVisibility(View.GONE);
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.VISIBLE);
//                        My_wallet_fragment();
                        my_leave();
                        break;

                    case 8:
                        mDrawerLayout.closeDrawers();
                        adnew_emplyTV.setVisibility(View.VISIBLE);
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        my_allowance();
                        break;

                    case 9:
                        mDrawerLayout.closeDrawers();
                        adnew_emplyTV.setVisibility(View.GONE);
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        login_webMicra();
                        break;

                    case 10:
                        mDrawerLayout.closeDrawers();
                        adnew_emplyTV.setVisibility(View.GONE);
                        notification.setVisibility(View.GONE);
                        search.setVisibility(View.GONE);
                        add_new.setVisibility(View.GONE);
                        editTV.setVisibility(View.GONE);
                        adnew_leaveTV.setVisibility(View.GONE);
                        settings();
                        break;

                    case 11:
                        mDrawerLayout.closeDrawers();
                        logout();
                        break;


                }
            }


        });
    }


    @Override
    public void onClick(View view) {
        if (view == menu) {
            mDrawerLayout.openDrawer(Gravity.LEFT); //Edit Gravity.START need API 14

        }
        if (view == adnew_emplyTV) {
            Intent i = new Intent(Main_screen.this, Add_allowance.class);
            startActivity(i);
            overridePendingTransition(R.anim.enter,R.anim.exit);
        }
        if (view == search) {

        }
        if (view == add_new) {
            Intent i = new Intent(Main_screen.this, Add_new_customers.class);
            startActivity(i);
            overridePendingTransition(R.anim.enter,R.anim.exit);
        }
        if (view == notification) {
            Intent i = new Intent(Main_screen.this, Notification_Activity.class);
            startActivity(i);
            overridePendingTransition(R.anim.enter,R.anim.exit);
        }
        if (view == editTV) {
            Intent i = new Intent(Main_screen.this, Edit_profile.class);
            startActivity(i);
            overridePendingTransition(R.anim.enter,R.anim.exit);

        }

        if (view == adnew_leaveTV) {
            Intent i = new Intent(Main_screen.this, leave_application_activitry.class);
            startActivity(i);
            overridePendingTransition(R.anim.enter,R.anim.exit);
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        }
    }

    private void my_profile() {

        Fragment favouriteFragment = new MyProfile_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment);
        fragmentTransaction.commit();


    }

    private void messages() {
        Fragment favouriteFragment = new MessageActivity();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment);
        fragmentTransaction.commit();
    }

    private void store_employee() {
        Fragment favouriteFragment = new Store_employee();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment);
        fragmentTransaction.commit();
    }

    private void my_leave() {
        Fragment favouriteFragment = new LeaveApplication_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment);
        fragmentTransaction.commit();

      /*  Intent i = new Intent(Main_screen.this, leave_application_activitry.class);
        startActivity(i);*/
    }

    private void my_allowance() {
        Fragment favouriteFragment = new MyAllowance_Activity();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment);
        fragmentTransaction.commit();
    }

    private void login_webMicra() {
        Fragment favouriteFragment = new WebMicra_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "Customers");
        fragmentTransaction.commit();

    }

    private void settings() {
        Fragment favouriteFragment = new Settings_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "Customers");
        fragmentTransaction.commit();

       /* Intent i = new Intent(Main_screen.this, About_us.class);
        startActivity(i);*/
    }

    private void logout() {
        Intent intent = new Intent(Main_screen.this, Login_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter,R.anim.exit);

    }

    private void home() {
        Fragment favouriteFragment = new SelectStore_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "Customers");
        fragmentTransaction.commit();
    /*    Intent i = new Intent(Main_screen.this, Customer_cart.class);
        startActivity(i);*/
    }

    private void omyorder_fragment() {
        Fragment favouriteFragment = new MyOrder_FRagment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "Customers");
        fragmentTransaction.commit();

    }


    private void my_customer() {

        Fragment favouriteFragment = new My_customer();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "Customers");
        fragmentTransaction.commit();
    }

    //    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    public boolean onMenuItemClick(MenuItem item) {
//                        return true;
//                    }
//                });
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
    public void settitleactivity(String title) {
        //    getSupportActionBar().setTitle(title);
        titletext.setText(title);
    }

    public void showicon(boolean b) {
        if (b) {
            notification.setVisibility(View.VISIBLE);
            search.setVisibility(View.VISIBLE);
            add_new.setVisibility(View.VISIBLE);
        } else {
            notification.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            add_new.setVisibility(View.GONE);
        }
    }

    public void addnewicon(boolean b) {
        if (b) {
            adnew_emplyTV.setVisibility(View.VISIBLE);
        } else {
            adnew_emplyTV.setVisibility(View.GONE);

        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Are you sure you want to exit?");
        alert.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finishAffinity();
            }
        }).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }
}
