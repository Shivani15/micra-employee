package com.relinns.micra.Activity;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra.Adapter.Message_Adapter;
import com.relinns.micra.Adapter.Notification_Adapter;
import com.relinns.micra.R;

public class Notification_Activity extends Activity implements View.OnClickListener {
    ListView employlistLV;
    RelativeLayout backRL;
    Notification_Adapter notification_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);
        employlistLV=(ListView)findViewById(R.id.employlistLV);
        notification_adapter=new Notification_Adapter(Notification_Activity.this);
        employlistLV.setAdapter(notification_adapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Notification_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
