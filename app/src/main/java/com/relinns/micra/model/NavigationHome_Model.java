package com.relinns.micra.model;

/**
 * Created by admin on 07-07-2017.
 */
public class NavigationHome_Model {
    Integer image;
    String name;
    public NavigationHome_Model(Integer image, String name) {
        this.image=image;
        this.name=name;

    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
